#!/usr/bin/env python

import logging
from datetime import datetime

import mnist
import numpy as np

from classifiers import mnist_naive_bayes
from classifiers import mnist_k_nearest_neighbors
from classifiers import mnist_decision_tree
from classifiers import mnist_random_forest
from classifiers import mnist_kernels
from classifiers import mnist_pca
from utils import print_title

logger = logging.getLogger('proj')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s: %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

TRAIN_EXAMPLES = 60000
TEST_EXAMPLES = 10000
FEATURES = 784

# All classifiers to be executed
CLASSIFIERS = [
    # mnist_naive_bayes,
    # mnist_k_nearest_neighbors
    # mnist_decision_tree,
    # mnist_random_forest,
    mnist_kernels
    # mnist_pca
]

def formatted_mnist_dataset():
    """Gathers and reshapes the MNIST dataset.

    The MNIST dataset is comprised of 70,000 images - each 28x28 resolution.
    These images represent hand-written digits 0-9.
    There are 60,000 training images, and 10,000 testing images, each with
    their own label.
    Each image is represented by a 2-D array with shape (28, 28).
    The dataset must be formatted so that each image is a 1-D array, appending
    subsequent rows to the first so that the image is flattened.
    Each pixel then represents a feature so the return shapes are as follows:

    Training examples shape: (60000, 784)
    Training labels shape: (60000,)
    Testing examples shape: (10000, 784)
    Testing labels shape: (10000,)
    """
    print("Retrieving MNIST Dataset...")
    x_train = mnist.train_images()
    y_train = mnist.train_labels()
    x_test = mnist.test_images()
    y_test = mnist.test_labels()

    x_train = np.reshape(x_train, (TRAIN_EXAMPLES, FEATURES))
    x_test = np.reshape(x_test, (TEST_EXAMPLES, FEATURES))

    logger.debug("Training examples shape: {}".format(x_train.shape))
    logger.debug("Training labels shape: {}".format(y_train.shape))
    logger.debug("Testing examples shape: {}".format(x_test.shape))
    logger.debug("Testing labels shape: {}".format(y_test.shape))
    print("MNIST Dataset retrieved and formatted.\n")

    return (x_train, y_train, x_test, y_test)

def execute_classifiers(classifiers, x_train, y_train, x_test, y_test):
    for c in classifiers:
        start_time = datetime.now()
        c(x_train, y_train, x_test, y_test)
        time_elapsed = datetime.now() - start_time
        print('Time elapsed (hh:mm:ss.ms) {}\n'.format(time_elapsed))

if __name__ == '__main__':
    print_title("MNIST Classification Accuracies with Machine Learning Techniques")
    x_train, y_train, x_test, y_test = formatted_mnist_dataset()
    execute_classifiers(CLASSIFIERS, x_train, y_train, x_test, y_test)
