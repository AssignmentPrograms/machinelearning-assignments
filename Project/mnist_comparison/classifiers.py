"""Classifier Module."""

from sklearn import neighbors
from sklearn import svm
from sklearn.naive_bayes import MultinomialNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import PCA

from utils import print_accuracy
from utils import print_title
from utils import store_model

# Constants
STORE_MODELS = True
MODEL_DIR = "../models"
K_NEAREST_NEIGHBORS = [3, 5, 7]
DT_MIN_LEAF_SAMPLES = [1, 5, 8, 20]
RF_NUM_ESTIMATORS = [1, 5, 10, 50, 70, 100]
KERNELS = ['poly']
COMPONENTS = [2,3,5,10]

def mnist_naive_bayes(x_train, y_train, x_test, y_test):
    print_title("Naive Bayes Accuracy")
    model = MultinomialNB()
    model.fit(x_train, y_train)
    y_pred = model.predict(x_test)
    print_accuracy(y_pred, y_test)

    if STORE_MODELS:
        file_prefix = "naive-bayes"
        store_model(model, file_prefix, MODEL_DIR)

def mnist_k_nearest_neighbors(x_train, y_train, x_test, y_test):
    print_title("K-Nearest Neighbors Accuracy with {} nearest "
          "neighbors".format(K_NEAREST_NEIGHBORS))

    for num in K_NEAREST_NEIGHBORS:
        print("K-Nearest Neighbors Accuracy: {} neighbor(s)".format(num))
        model = neighbors.KNeighborsClassifier(n_neighbors=num)
        model.fit(x_train, y_train).predict(x_test)
        y_pred = model.predict(x_test)
        print_accuracy(y_pred, y_test)

        if STORE_MODELS:
            file_prefix = "knn-{}neighbors".format(num)
            store_model(model, file_prefix, MODEL_DIR)

def mnist_decision_tree(x_train, y_train, x_test, y_test):
    print_title("Decision Tree Accuracy with {} samples".format(DT_MIN_LEAF_SAMPLES))

    for l_val in DT_MIN_LEAF_SAMPLES:
        print("Decision Tree Accuracy: {} sample(s) at leaf".format(l_val))
        model = DecisionTreeClassifier(min_samples_leaf=l_val)
        model.fit(x_train, y_train)
        y_pred = model.predict(x_test)
        print_accuracy(y_pred, y_test)

        if STORE_MODELS:
            file_prefix = "dt-{}samples".format(l_val)
            store_model(model, file_prefix, MODEL_DIR)

def mnist_random_forest(x_train, y_train, x_test, y_test):
    print_title("Random Forest Accuracy with {} estimators".format(RF_NUM_ESTIMATORS))

    for r_val in RF_NUM_ESTIMATORS:
        print("Random Forest Accuracy: {} estimators".format(r_val))
        model = RandomForestClassifier(n_estimators=r_val)
        model.fit(x_train, y_train)
        y_pred = model.predict(x_test)
        print_accuracy(y_pred, y_test)

        if STORE_MODELS:
            file_prefix = "rf-{}estimators".format(r_val)
            store_model(model, file_prefix, MODEL_DIR)

def mnist_kernels(x_train, y_train, x_test, y_test):
    gamma = 2
    print_title("Kernel method with {} kernels and gamma={}".format(KERNELS, gamma))

    for kernel in KERNELS:
        print("Kernel Accuracy: {} kernel".format(kernel))
        model = svm.SVC(kernel=kernel, gamma=gamma)
        model.fit(x_train, y_train)
        y_pred = model.predict(x_test)
        print_accuracy(y_pred, y_test)

        if STORE_MODELS:
            file_prefix = "kernel-{}".format(kernel)
            store_model(model, file_prefix, MODEL_DIR)

def mnist_pca(x_train, y_train, x_test, y_test):
    print_title("PCA with {} components".format(COMPONENTS))

    for num_components in COMPONENTS:
        print("PCA Accuracy: {} components".format(num_components))
        pca = PCA(n_components=num_components, svd_solver='full')
        pca.fit_transform(x_train)

        x_transformed_train = pca.transform(x_train)
        x_transformed_test = pca.transform(x_test)

        print("length before = {}\nLength after = {}".format(len(x_train), len(x_transformed_train)))

        model = svm.SVC(kernel='linear', gamma=2)
        model.fit(x_transformed_train, y_train)



        #y_pred = model.predict(x_test)
        #print_accuracy(y_pred, y_test)

        '''if STORE_MODELS:
            file_prefix = "pca-{}".format(num_components)
            store_model(model, file_prefix, MODEL_DIR)'''














