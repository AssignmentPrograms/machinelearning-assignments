"""Utility Module."""

import os
import pickle
from datetime import datetime

TITLE_SEPARATOR = "-"
DATETIME_FORMAT = "%Y-%m-%d_%H-%M-%S"
PICKLE_FILE_EXTENSION = "obj"

def print_title(title):
    print(title)
    print(TITLE_SEPARATOR * len(title))

def print_accuracy(y_pred, y_test):
    total = len(y_test)
    correct = sum(a == b for a,b in zip(y_test, y_pred))
    accuracy = float(correct) / float(total) * 100
    error_pct = 100 - accuracy

    print(" -- Total Test Examples: %d" % (total))
    print(" -- Correctly Predicted: %d" % (correct))
    print(" -- Accuracy: %.4f%%" % (accuracy))
    print(" -- Error Percentage: %.4f%%" % (error_pct))

def store_model(model, file_prefix, directory):
    filename = "{}_{}.{}".format(file_prefix, get_current_datetime_str(),
                                 PICKLE_FILE_EXTENSION)
    filepath = os.path.join(directory, filename)

    print("Storing model to '{}'".format(filepath))
    filehandler = open(filepath, 'w')
    pickle.dump(model, filehandler)

def load_model(filepath):
    print("Loading model from '{}'".format(filepath))
    filehandler = open(filepath, 'r')
    return pickle.load(filehandler)

def get_current_datetime_str():
    curr = datetime.now()
    return curr.strftime(DATETIME_FORMAT)
