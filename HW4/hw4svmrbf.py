
import pandas as pd
import numpy as np
from sklearn import svm
from sklearn.model_selection import cross_val_score


def cross_validate( estimator, training_set, labels, num_folds, cv_subject ):
    final_val = 0
    highest_avg_cv_accuracy = 0

    for val in cv_subject:
        clf.C = val
        cross_val_accuracy = cross_val_score( estimator=estimator,
                                              X=training_set,
                                              y=labels,
                                              cv=num_folds
                                            )

        avg_cv_accuracy = np.mean(cross_val_accuracy)
        if highest_avg_cv_accuracy < avg_cv_accuracy:
            highest_avg_cv_accuracy = avg_cv_accuracy
            final_val = val

        print("Current parameter value = {}".format( val ))
        print("cross_val_accuracy = {}".format( cross_val_accuracy ))
        print("Average cv accuracy = {}".format( avg_cv_accuracy ))
        print("\n")
    
    print("Chosen regularization parameter = {}".format( final_val ))
    return final_val


def split_list(toSplit, splitAt):
    first_half = [ x[:splitAt] for x in toSplit ]
    second_half = [ x[splitAt:] for x in toSplit ]

    return first_half, second_half


training_set = pd.read_csv('zip.train', delim_whitespace=True, skipinitialspace=True, header=None)
testing_set = pd.read_csv('zip.test', delim_whitespace=True, skipinitialspace=True, header=None)

training_set = training_set.values
testing_set = testing_set.values

#training_set = training_set.reshape(training_set.shape[0])
#testing_set = testing_set.reshape(testing_set.shape[0])

Y_train, X_train = split_list( training_set, 1 )
Y_train = [label[0] for label in Y_train]
#print X_train
#print Y_train


Y_test, X_test = split_list( testing_set, 1 )
Y_test = [label[0] for label in Y_test]


clf = svm.SVC( kernel='rbf' )


clf.C = cross_validate( estimator=clf, 
                        #parameter=clf.C,
                        training_set=X_train,
                        labels=Y_train, 
                        num_folds=5, 
                        cv_subject=[0.001, 0.01, 0.1, 1, 10, 100, 1000]
                      )

clf.fit( X_train, Y_train )

accuracy = clf.score( X_test, Y_test )
print("Computed test accuracy = {}".format( accuracy ))

