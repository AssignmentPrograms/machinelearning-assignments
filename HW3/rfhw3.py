
import pandas as pd
from sklearn.ensemble import RandomForestClassifier

def split_list(toSplit, splitAt):
    first_half = [ x[:splitAt] for x in toSplit ]
    second_half = [ x[splitAt:] for x in toSplit ]

    return first_half, second_half


training_set = pd.read_csv('zip.train', delim_whitespace=True, skipinitialspace=True, header=None)
testing_set = pd.read_csv('zip.test', delim_whitespace=True, skipinitialspace=True, header=None)

training_set = training_set.values
testing_set = testing_set.values

#training_set = training_set.reshape(training_set.shape[0])
#testing_set = testing_set.reshape(testing_set.shape[0])

Y_train, X_train = split_list( training_set, 1 )
Y_train = [label[0] for label in Y_train]
#print X_train
#print Y_train


Y_test, X_test = split_list( testing_set, 1 )
Y_test = [label[0] for label in Y_test]

for r_val in [1, 5, 10, 50, 100, 150, 200]:
    print("Value for number of trees in forest set to... {}".format(r_val))

    rfClassifier = RandomForestClassifier(n_estimators=r_val)

    rfClassifier.fit(X_train, Y_train)

    Y_pred = rfClassifier.predict(X_test)

    misclassified = [actual for actual, predicted in zip(Y_test, Y_pred) if actual != predicted]

    print("Number misclassified = {} out of {} examples".format(len(misclassified), len(Y_test)))

    print("Error with number of random forest={} is... {}".format(r_val, len(misclassified) / float(len(Y_test))))
    print("\n\n\n")
