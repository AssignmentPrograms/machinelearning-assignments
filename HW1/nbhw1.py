import pandas as pd
from sklearn.naive_bayes import MultinomialNB

spam_testing_labels = pd.read_csv('test-labels.txt', header=None)
spam_training_data = pd.read_csv('spam-training.txt', header=None)
spam_training_labels = pd.read_csv('train-labels.txt', header=None)
spam_testing_data = pd.read_csv('spam-testing.txt', header=None)

#print(tmp)

spam_testing_labels = spam_testing_labels.values
spam_training_labels = spam_training_labels.values
#print(tmp1)

spam_testing_labels = spam_testing_labels.reshape(spam_testing_labels.shape[0])
spam_training_labels = spam_training_labels.reshape(spam_training_labels.shape[0])
print(spam_testing_labels)
print(spam_training_data.values)
print(spam_testing_data.values)

bnb = MultinomialNB()

y_pred = bnb.fit(spam_training_data.values, spam_training_labels).predict(spam_testing_data.values)
print("Result = {}".format(y_pred))
print("Actual Result = {}".format(spam_testing_labels))

misclassified = [actual for actual, predicted in zip(y_pred, spam_testing_labels) if actual != predicted]

print("List of misclassifications: {}".format(misclassified))

print("Ratio of misclassification = {}/{} = {}".format(len(misclassified), len(spam_testing_labels), len(misclassified) / float(len(spam_testing_labels))))

